﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using HtmlAgilityPack;
using Timer = System.Windows.Forms.Timer;

namespace calendarApp2
{
    public partial class Form1 : Form
    {

        private const string loginUrl = "https://servicos.dpf.gov.br/sinpa/realizarReagendamento.do";
        private const string calendarUrl = "https://servicos.dpf.gov.br/sinpa/realizarAgendamento.do";
        public Settings settings;
        public ProfileManager manager;
        private Timer timer;
        public Form1()
        {
            InitializeComponent();
            settings = new Settings();
            settings.load();
            
            timer = new Timer();
            timer.Tick += Timer_Tick;
            intervalBox.Text = settings.settings.refreshTimer;
            timer.Interval = Int32.Parse(intervalBox.Text) * 1000;
            timer.Start();
            manager = new ProfileManager(settings.settings.profilesFilename);
            manager.load();
        }

        private async void Timer_Tick(object sender, EventArgs e)
        {
            try
            {
                foreach (var managerProfile in manager.profiles)
                {
                    if (managerProfile.needToCheck)
                    {
                        bool flag = await Task.Run(() => getDatasForProfile(managerProfile));
                        if (flag)
                            doNotify(managerProfile);
                    }
                }

                refreshBoxes();
            }
            catch
            { }

        }

        private void refreshBoxes()
        {
            changeBox.Items.Clear();
            datasBox.Items.Clear();
            if (profilesBox.SelectedItems.Count==0)
                return;
            Profile profile = manager.profiles.FirstOrDefault(p => p.ProfileName == profilesBox.SelectedItems[0].Text);
            if (profile == null)
                return;
            foreach (var historyEvent in profile.History)
            {
                changeBox.Items.Add(historyEvent.Description + " ( " + historyEvent.EventTime.ToString("G") + " ) ");
            }
            
            foreach (var profileData in profile.Datas)
            {
                datasBox.Items.Add(profileData);
            }
            monthCalendar1.BoldedDates = profile.Datas.ToArray();
            lastTime.Text = profile.lastUpdate.ToString("G");
            observBox.Text = profile.Observation;
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            foreach (var profile in manager.profiles)
            {
                profilesBox.Items.Add(profile.ProfileName);
            }
        }

        private void refreshProfilesFormManager()
        {
            profilesBox.Items.Clear();
            foreach (var profile in manager.profiles)
            {
                profilesBox.Items.Add(profile.ProfileName);
            }
        }
        private void Settings_Click(object sender, EventArgs e)
        {
            SettingsForm form = new SettingsForm(this);
            form.ShowDialog();
            manager.load();
            refreshProfilesFormManager();
        }

        private void profilesBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            refreshBoxes();
        }
        public bool getDatasForProfile(Profile profile)
        {
            bool flag = false;
            using (WebScraper ws = new WebScraper())
            {
                var reqparm = new NameValueCollection
                {
                    {"dispatcher", "processarConsultaAgendamento"},
                    {"validate", "true"},
                    {"operacao", "reagendar"},
                    {"cpf", profile.CPF},
                    {"protocolo", profile.ProtocolNumber},
                    {"dataNascimento", profile.BirthData},
                    {"email1", ""},
                    {"email2", ""},
                    {"url", ""}
                };
                byte[] responsebytes = ws.UploadValues(loginUrl, "POST", reqparm);
                string responsebody = Encoding.UTF8.GetString(responsebytes);
                reqparm = new NameValueCollection
                {
                    {"dispatcher", "exibirInclusaoAgendamento"},
                    {"validate", "false"},
                    {"postoId", profile.Location.PostId}, //id центра выдачи
                    {"ufPosto", profile.Location.State}, //state
                    {"cidadePosto", profile.Location.CityId}, //тоже какой-то id
                };
                responsebytes = ws.UploadValues(calendarUrl, "POST", reqparm);
                responsebody = Encoding.UTF8.GetString(responsebytes);
                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                doc.LoadHtml(responsebody);
                var root = doc.DocumentNode;
                List<DateTime> oldData = profile.Datas;
                List<DateTime> newData = new List<DateTime>();
                foreach (HtmlNode node in root.Descendants("option"))
                {
                    if (node.Attributes["value"].Value != "")
                    {
                        newData.Add(DateTime.Parse(node.Attributes["value"].Value));
                    }
                    profile.lastUpdate = DateTime.Now;
                }

                List<DateTime> changes = newData.Except(oldData).ToList();

                foreach (var dateTime in changes)
                {
                    profile.History.Insert(0, new CalendarEvent()
                    {
                        Description = "added new day:" + dateTime.Date.ToString("d"),
                        EventTime = DateTime.Now
                    });
                }

                if (changes.Count != 0)
                {
                    profile.Datas.Clear();
                    foreach (var dateTime in newData)
                    {
                        profile.Datas.Add(dateTime);
                    }

                    manager.save();

                    if (oldData.Count != 0)
                    {
                        //doNotify(profile);
                        flag = true;
                    }

                }


            }

            return flag;
        }

        private void doNotify(Profile profile)
        {
            new NotifyForm(profile).Show();
            try
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.Alert);
                simpleSound.Play();
            }
            catch (Exception e)
            {
                File.AppendAllText("log.txt", e.Message + "\n");
            }

            foreach (ListViewItem item in profilesBox.Items)
            {
                if (item.Text == profile.ProfileName)
                    item.BackColor = Color.Red;
            }

        }
        private void setintervalButton_Click(object sender, EventArgs e)
        {
            timer.Interval = Int32.Parse(intervalBox.Text) * 1000;
            settings.settings.refreshTimer = intervalBox.Text;
            settings.save();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (profilesBox.SelectedItems.Count != 0)
            {
                profilesBox.SelectedItems[0].BackColor=Color.White;
                new NotifyForm(manager.profiles.FirstOrDefault(p => p.ProfileName == profilesBox.SelectedItems[0].Text))
                    .Show();

            }
        }
        private void Form1_Deactivate(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.ShowInTaskbar = false;
                notifyIcon1.Visible = true;
            }
        }
        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.WindowState = FormWindowState.Normal;
                this.ShowInTaskbar = true;
                notifyIcon1.Visible = false;
            }
        }
    }
}
