﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace calendarApp2
{
    public partial class NotifyForm : Form
    {
        private const string loginUrl = "https://servicos.dpf.gov.br/sinpa/realizarReagendamento.do";
        private Profile profile;
        private CookieContainer cookie;
        public NotifyForm(Profile profile)
        {
            InitializeComponent();
            this.profile = profile;
            messageLabel.Text = "Profile has new date available: " + profile.ProfileName;
        }
        private void NotifyForm_Load(object sender, EventArgs e)
        {

        }
        private void button1_Click(object sender, EventArgs e)
        {
            IWebDriver browser = new OpenQA.Selenium.Chrome.ChromeDriver();
            browser.Navigate().GoToUrl("https://servicos.dpf.gov.br/sinpa/realizarReagendamento.do?dispatcher=exibirSolicitacaoReagendamento&validate=false");
            IWebElement cpf = browser.FindElement(By.Name("cpf"));
            IWebElement protocolo = browser.FindElement(By.Name("protocolo"));
            IWebElement dataNascimento = browser.FindElement(By.Name("dataNascimento"));
            cpf.SendKeys(profile.CPF);
            protocolo.SendKeys(profile.ProtocolNumber);
            dataNascimento.SendKeys(profile.BirthData);
            this.Close();
            #region
            /*
             * using (WebScraper ws = new WebScraper())
            {
                var reqparm = new NameValueCollection
                {
                    {"dispatcher", "processarConsultaAgendamento"},
                    {"validate", "true"},
                    {"operacao", "reagendar"},
                    {"cpf", profile.CPF},
                    {"protocolo", profile.ProtocolNumber},
                    {"dataNascimento", profile.BirthData},
                    {"email1", ""},
                    {"email2", ""},
                    {"url", ""}
                };
                byte[] responsebytes = ws.UploadValues(loginUrl, "POST", reqparm);
                cookie = ws.m_container;
                string responsebody = Encoding.UTF8.GetString(responsebytes);
            }

            List<Cookie> cl = cookie.GetAllCookies().ToList();
            Cookie temp1 = cl[0];
            new BrowserForm(profile, temp1).Show();
            this.Close();
            */
            #endregion
        }
    }
}
