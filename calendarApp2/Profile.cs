﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace calendarApp2
{
    public class ProfileManager
    {
        private string filename;

        public ProfileManager(string filename)
        {
            this.filename = filename;
        }

        public List<Profile> profiles;

        public void load()
        {
            profiles = File.Exists(filename)
                ? JsonConvert.DeserializeObject<List<Profile>>(File.ReadAllText(filename))
                : new List<Profile>();
        }

        public void save()
        {
            File.WriteAllText(filename, JsonConvert.SerializeObject(profiles));
        }
    }


    public class Profile
    {
        public bool needToCheck { get; set; }
        public string ProfileName { get; set; }
        public string CPF { get; set; }
        public string ProtocolNumber { get; set; }
        public string BirthData { get; set; }
        public string Observation { get; set; }
        public DateTime lastUpdate { get; set; }
        public List<DateTime> Datas { get; set; }
        public List<CalendarEvent> History { get; set; }
        public Location Location { get; set; }

    }

    public class Location
    {
        public string State { get; set; }
        public string City { get; set; }
        public string CityId { get; set; }
        public string PostId { get; set; }
        public string PostoName { get; set; }
    }
    public class CalendarEvent
    {
        public DateTime EventTime { get; set; }
        public string Description { get; set; }
    }
}
