﻿namespace calendarApp2
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.changeBox = new System.Windows.Forms.ListBox();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.Settings = new System.Windows.Forms.Button();
            this.datasBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.intervalBox = new System.Windows.Forms.TextBox();
            this.setintervalButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.lastTime = new System.Windows.Forms.Label();
            this.observBox = new System.Windows.Forms.TextBox();
            this.profilesBox = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // changeBox
            // 
            this.changeBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.changeBox.FormattingEnabled = true;
            this.changeBox.Items.AddRange(new object[] {
            "changes history"});
            this.changeBox.Location = new System.Drawing.Point(361, 35);
            this.changeBox.Name = "changeBox";
            this.changeBox.Size = new System.Drawing.Size(323, 290);
            this.changeBox.TabIndex = 1;
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(696, 34);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 4;
            // 
            // Settings
            // 
            this.Settings.Location = new System.Drawing.Point(784, 305);
            this.Settings.Name = "Settings";
            this.Settings.Size = new System.Drawing.Size(75, 23);
            this.Settings.TabIndex = 6;
            this.Settings.Text = "Settings";
            this.Settings.UseVisualStyleBackColor = true;
            this.Settings.Click += new System.EventHandler(this.Settings_Click);
            // 
            // datasBox
            // 
            this.datasBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.datasBox.FormattingEnabled = true;
            this.datasBox.Location = new System.Drawing.Point(206, 34);
            this.datasBox.Name = "datasBox";
            this.datasBox.Size = new System.Drawing.Size(149, 290);
            this.datasBox.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(207, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 20);
            this.label1.TabIndex = 8;
            this.label1.Text = "List of dates";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(2, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 20);
            this.label2.TabIndex = 9;
            this.label2.Text = "Profiles";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(357, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 20);
            this.label3.TabIndex = 10;
            this.label3.Text = "Changes history";
            // 
            // intervalBox
            // 
            this.intervalBox.Location = new System.Drawing.Point(696, 209);
            this.intervalBox.MaxLength = 2;
            this.intervalBox.Name = "intervalBox";
            this.intervalBox.Size = new System.Drawing.Size(163, 20);
            this.intervalBox.TabIndex = 11;
            this.intervalBox.Text = "60";
            // 
            // setintervalButton
            // 
            this.setintervalButton.Location = new System.Drawing.Point(696, 235);
            this.setintervalButton.Name = "setintervalButton";
            this.setintervalButton.Size = new System.Drawing.Size(163, 23);
            this.setintervalButton.TabIndex = 12;
            this.setintervalButton.Text = "Set interval";
            this.setintervalButton.UseVisualStyleBackColor = true;
            this.setintervalButton.Click += new System.EventHandler(this.setintervalButton_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(69, 9);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(131, 23);
            this.button1.TabIndex = 13;
            this.button1.Text = "Go to shedule in browser";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.DoubleClick += new System.EventHandler(this.notifyIcon1_DoubleClick);
            // 
            // lastTime
            // 
            this.lastTime.AutoSize = true;
            this.lastTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lastTime.Location = new System.Drawing.Point(727, 9);
            this.lastTime.Name = "lastTime";
            this.lastTime.Size = new System.Drawing.Size(46, 17);
            this.lastTime.TabIndex = 14;
            this.lastTime.Text = "label4";
            // 
            // observBox
            // 
            this.observBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.observBox.Location = new System.Drawing.Point(12, 304);
            this.observBox.Name = "observBox";
            this.observBox.ReadOnly = true;
            this.observBox.Size = new System.Drawing.Size(188, 21);
            this.observBox.TabIndex = 15;
            // 
            // profilesBox
            // 
            this.profilesBox.Location = new System.Drawing.Point(6, 35);
            this.profilesBox.MultiSelect = false;
            this.profilesBox.Name = "profilesBox";
            this.profilesBox.Size = new System.Drawing.Size(194, 263);
            this.profilesBox.TabIndex = 16;
            this.profilesBox.UseCompatibleStateImageBehavior = false;
            this.profilesBox.View = System.Windows.Forms.View.List;
            this.profilesBox.SelectedIndexChanged += new System.EventHandler(this.profilesBox_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(878, 340);
            this.Controls.Add(this.profilesBox);
            this.Controls.Add(this.observBox);
            this.Controls.Add(this.lastTime);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.setintervalButton);
            this.Controls.Add(this.intervalBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.datasBox);
            this.Controls.Add(this.Settings);
            this.Controls.Add(this.monthCalendar1);
            this.Controls.Add(this.changeBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Deactivate += new System.EventHandler(this.Form1_Deactivate);
            this.Shown += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox changeBox;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.Button Settings;
        private System.Windows.Forms.ListBox datasBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox intervalBox;
        private System.Windows.Forms.Button setintervalButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.Label lastTime;
        private System.Windows.Forms.TextBox observBox;
        private System.Windows.Forms.ListView profilesBox;
    }
}

