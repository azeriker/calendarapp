﻿namespace calendarApp2
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.profilesBox = new System.Windows.Forms.ListBox();
            this.profileBox = new System.Windows.Forms.TextBox();
            this.observBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.stateBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cityBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.saveButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.postBox = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.oldCity = new System.Windows.Forms.Label();
            this.oldPosto = new System.Windows.Forms.Label();
            this.oldState = new System.Windows.Forms.Label();
            this.groupBox = new System.Windows.Forms.GroupBox();
            this.cpfBox = new System.Windows.Forms.MaskedTextBox();
            this.protocolBox = new System.Windows.Forms.MaskedTextBox();
            this.birthBox = new System.Windows.Forms.MaskedTextBox();
            this.checkBox = new System.Windows.Forms.CheckBox();
            this.groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // profilesBox
            // 
            this.profilesBox.FormattingEnabled = true;
            this.profilesBox.Location = new System.Drawing.Point(13, 13);
            this.profilesBox.Name = "profilesBox";
            this.profilesBox.Size = new System.Drawing.Size(147, 290);
            this.profilesBox.TabIndex = 0;
            this.profilesBox.SelectedIndexChanged += new System.EventHandler(this.profilesBox_SelectedIndexChanged);
            // 
            // profileBox
            // 
            this.profileBox.Location = new System.Drawing.Point(167, 13);
            this.profileBox.Name = "profileBox";
            this.profileBox.Size = new System.Drawing.Size(100, 20);
            this.profileBox.TabIndex = 1;
            // 
            // observBox
            // 
            this.observBox.Location = new System.Drawing.Point(167, 121);
            this.observBox.Name = "observBox";
            this.observBox.Size = new System.Drawing.Size(100, 20);
            this.observBox.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(273, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Profile name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(273, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "CPF";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(273, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Protocol";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(273, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Birth date";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(273, 124);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Observation";
            // 
            // stateBox
            // 
            this.stateBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.stateBox.FormattingEnabled = true;
            this.stateBox.Location = new System.Drawing.Point(167, 148);
            this.stateBox.Name = "stateBox";
            this.stateBox.Size = new System.Drawing.Size(100, 21);
            this.stateBox.TabIndex = 11;
            this.stateBox.SelectedIndexChanged += new System.EventHandler(this.stateBox_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(273, 151);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "State";
            // 
            // cityBox
            // 
            this.cityBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cityBox.FormattingEnabled = true;
            this.cityBox.Location = new System.Drawing.Point(167, 176);
            this.cityBox.Name = "cityBox";
            this.cityBox.Size = new System.Drawing.Size(100, 21);
            this.cityBox.TabIndex = 13;
            this.cityBox.SelectedIndexChanged += new System.EventHandler(this.cityBox_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(273, 179);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "City";
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(302, 237);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(91, 33);
            this.saveButton.TabIndex = 15;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(410, 237);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(91, 33);
            this.deleteButton.TabIndex = 16;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(167, 237);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(91, 23);
            this.clearButton.TabIndex = 18;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // postBox
            // 
            this.postBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.postBox.FormattingEnabled = true;
            this.postBox.Location = new System.Drawing.Point(167, 204);
            this.postBox.Name = "postBox";
            this.postBox.Size = new System.Drawing.Size(139, 21);
            this.postBox.TabIndex = 19;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(312, 207);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "Posto";
            // 
            // oldCity
            // 
            this.oldCity.AutoSize = true;
            this.oldCity.Location = new System.Drawing.Point(6, 55);
            this.oldCity.Name = "oldCity";
            this.oldCity.Size = new System.Drawing.Size(41, 13);
            this.oldCity.TabIndex = 22;
            this.oldCity.Text = "label10";
            // 
            // oldPosto
            // 
            this.oldPosto.AutoSize = true;
            this.oldPosto.Location = new System.Drawing.Point(6, 83);
            this.oldPosto.Name = "oldPosto";
            this.oldPosto.Size = new System.Drawing.Size(41, 13);
            this.oldPosto.TabIndex = 23;
            this.oldPosto.Text = "label11";
            // 
            // oldState
            // 
            this.oldState.AutoSize = true;
            this.oldState.Location = new System.Drawing.Point(6, 27);
            this.oldState.Name = "oldState";
            this.oldState.Size = new System.Drawing.Size(41, 13);
            this.oldState.TabIndex = 24;
            this.oldState.Text = "label12";
            // 
            // groupBox
            // 
            this.groupBox.Controls.Add(this.oldState);
            this.groupBox.Controls.Add(this.oldPosto);
            this.groupBox.Controls.Add(this.oldCity);
            this.groupBox.Location = new System.Drawing.Point(352, 124);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(121, 100);
            this.groupBox.TabIndex = 25;
            this.groupBox.TabStop = false;
            this.groupBox.Text = "Old Values";
            // 
            // cpfBox
            // 
            this.cpfBox.Culture = new System.Globalization.CultureInfo("en-US");
            this.cpfBox.Location = new System.Drawing.Point(167, 40);
            this.cpfBox.Mask = "000.000.000-00";
            this.cpfBox.Name = "cpfBox";
            this.cpfBox.Size = new System.Drawing.Size(100, 20);
            this.cpfBox.TabIndex = 26;
            // 
            // protocolBox
            // 
            this.protocolBox.Culture = new System.Globalization.CultureInfo("en-US");
            this.protocolBox.Location = new System.Drawing.Point(167, 67);
            this.protocolBox.Mask = "0.0000.0000000000";
            this.protocolBox.Name = "protocolBox";
            this.protocolBox.Size = new System.Drawing.Size(100, 20);
            this.protocolBox.TabIndex = 27;
            // 
            // birthBox
            // 
            this.birthBox.Culture = new System.Globalization.CultureInfo("en-US");
            this.birthBox.Location = new System.Drawing.Point(167, 94);
            this.birthBox.Mask = "00/00/0000";
            this.birthBox.Name = "birthBox";
            this.birthBox.Size = new System.Drawing.Size(100, 20);
            this.birthBox.TabIndex = 28;
            this.birthBox.ValidatingType = typeof(System.DateTime);
            // 
            // checkBox
            // 
            this.checkBox.AutoSize = true;
            this.checkBox.Location = new System.Drawing.Point(361, 16);
            this.checkBox.Name = "checkBox";
            this.checkBox.Size = new System.Drawing.Size(95, 17);
            this.checkBox.TabIndex = 29;
            this.checkBox.Text = "need to check";
            this.checkBox.UseVisualStyleBackColor = true;
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(503, 311);
            this.Controls.Add(this.checkBox);
            this.Controls.Add(this.birthBox);
            this.Controls.Add(this.protocolBox);
            this.Controls.Add(this.cpfBox);
            this.Controls.Add(this.groupBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.postBox);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cityBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.stateBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.observBox);
            this.Controls.Add(this.profileBox);
            this.Controls.Add(this.profilesBox);
            this.Name = "SettingsForm";
            this.Text = "SettingsForm";
            this.groupBox.ResumeLayout(false);
            this.groupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox profilesBox;
        private System.Windows.Forms.TextBox profileBox;
        private System.Windows.Forms.TextBox observBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox stateBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cityBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.ComboBox postBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label oldCity;
        private System.Windows.Forms.Label oldPosto;
        private System.Windows.Forms.Label oldState;
        private System.Windows.Forms.GroupBox groupBox;
        private System.Windows.Forms.MaskedTextBox cpfBox;
        private System.Windows.Forms.MaskedTextBox protocolBox;
        private System.Windows.Forms.MaskedTextBox birthBox;
        private System.Windows.Forms.CheckBox checkBox;
    }
}