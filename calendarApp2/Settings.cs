﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace calendarApp2
{
    public class Settings
    {
        public Params settings;
        private const string settingsFilename = "appsettings.json";
        public void load()
        {
            if (File.Exists(settingsFilename))
            {
                settings = JsonConvert.DeserializeObject<Params>(File.ReadAllText(settingsFilename));
            }
            else
            {
                settings = new Params()
                {
                    profilesFilename = "profiles.json",
                    refreshTimer = "60",
                    useSecondWay = false
                };
                File.WriteAllText(settingsFilename, JsonConvert.SerializeObject(settings));
            }
        }

        public void save()
        {
            File.WriteAllText(settingsFilename, JsonConvert.SerializeObject(settings));
        }
    }
    public class Params
    {
        public string profilesFilename { get; set; }
        public string refreshTimer { get; set; }
        public bool useSecondWay { get; set; }
    }

}
