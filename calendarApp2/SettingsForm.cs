﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using HtmlAgilityPack;

namespace calendarApp2
{
    public partial class SettingsForm : Form
    {
        private Form1 parent;

        public SettingsForm(Form1 parent)
        {
            this.parent = parent;
            InitializeComponent();
            cityBox.DisplayMember = "name";
            cityBox.ValueMember = "value";
            foreach (var profile in parent.manager.profiles)
            {
                profilesBox.Items.Add(profile.ProfileName);
            }

            foreach (var value in Enum.GetValues(typeof(State)))
            {
                stateBox.Items.Add(value);
            }
        }

        private void profilesBox_SelectedIndexChanged(object sender, EventArgs e)
        {

            string profileName = (string)profilesBox.SelectedItem;

            if (profileName == null)
                return;
            Profile profile = parent.manager.profiles.FirstOrDefault(p => p.ProfileName == profileName);
            profileBox.Text = profile.ProfileName;
            birthBox.Text = profile.BirthData;
            cpfBox.Text = profile.CPF;
            protocolBox.Text = profile.ProtocolNumber;
            observBox.Text = profile.Observation;
            checkBox.Checked = profile.needToCheck;
            try
            {
                oldState.Text = profile.Location.State;
                oldCity.Text = profile.Location.City;
                oldPosto.Text = profile.Location.PostoName;
            }
            catch { }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (isValid())
            {
                Profile profile = parent.manager.profiles.FirstOrDefault(p => p.ProfileName == profileBox.Text);
                Profile newProfile = new Profile()
                {
                    BirthData = birthBox.Text,
                    CPF = cpfBox.Text,
                    Observation = observBox.Text,
                    ProfileName = profileBox.Text,
                    ProtocolNumber = protocolBox.Text,
                    needToCheck = checkBox.Checked,
                    Datas = new List<DateTime>(),
                    History = new List<CalendarEvent>(),
                    Location = new Location()
                    {
                        State = stateBox.Text,
                        City = cityBox.Text,
                        CityId = ((ajaxresponseItem)cityBox.SelectedItem).value,
                        PostId = ((KeyValuePair<string, string>)postBox.SelectedItem).Value,
                        PostoName = ((KeyValuePair<string, string>)postBox.SelectedItem).Key
                    }
                };
                if (profile == null)
                {
                    parent.manager.profiles.Add(newProfile);
                }
                else
                {
                    int index = parent.manager.profiles.FindIndex(p => p.ProfileName == profileBox.Text);
                    parent.manager.profiles.Remove(profile);
                    parent.manager.profiles.Insert(index, newProfile);
                }
                if (!profilesBox.Items.Contains(newProfile.ProfileName))
                    profilesBox.Items.Add(newProfile.ProfileName);
            }
            else
            {
                MessageBox.Show("Field is empty");
            }

            parent.manager.save();
        }

        private bool isValid()
        {
            return !(string.IsNullOrWhiteSpace(profileBox.Text) ||
                     string.IsNullOrWhiteSpace(birthBox.Text) ||
                     string.IsNullOrWhiteSpace(observBox.Text) ||
                     string.IsNullOrWhiteSpace(cpfBox.Text) ||
                     string.IsNullOrWhiteSpace(protocolBox.Text) ||
                     string.IsNullOrWhiteSpace(cityBox.Text) ||
                     string.IsNullOrWhiteSpace(stateBox.Text));
        }
        private void deleteButton_Click(object sender, EventArgs e)
        {
            Profile profileToDelete =
                parent.manager.profiles.FirstOrDefault(p => p.ProfileName == (string)profilesBox.SelectedItem);
            profilesBox.SelectedIndex = -1;
            profilesBox.Items.Remove(profileToDelete.ProfileName);
            parent.manager.profiles.Remove(profileToDelete);
            parent.manager.save();

        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            checkBox.Checked = false;
            profileBox.Text = "";
            birthBox.Text = "";
            cpfBox.Text = "";
            protocolBox.Text = "";
            observBox.Text = "";
            stateBox.SelectedIndex = -1;
            cityBox.Items.Clear();
            postBox.Items.Clear();
        }

        private void stateBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (WebClient wc = new WebClient())
            {
                string stateUrl = @"https://servicos.dpf.gov.br/sinpa/listarCidadesPostos.do?ufPosto=" + stateBox.SelectedItem;
                string response = wc.DownloadString(stateUrl);
                XmlSerializer serializer = new XmlSerializer(typeof(ajaxresponse));
                MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(response));
                ajaxresponse message = (ajaxresponse)serializer.Deserialize(memStream);
                cityBox.Items.Clear();
                foreach (var ajaxresponseItem in message.response)
                {
                    cityBox.Items.Add(ajaxresponseItem);
                }
            }
        }
        private void cityBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string cityUrl =
                @"https://servicos.dpf.gov.br/sinpa/realizarAgendamento.do?dispatcher=carregaPostos&municipio=" + ((ajaxresponseItem)cityBox.SelectedItem).value;
            HtmlWeb hw = new HtmlWeb();
            var doc = hw.Load(cityUrl);
            var root = doc.DocumentNode;
            postBox.Items.Clear();
            var keyval = new List<KeyValuePair<string, string>>();
            foreach (var node in root.Descendants("a"))
            {
                keyval.Add(new KeyValuePair<string, string>(node.InnerText, node.Attributes["href"].Value.Split('\'')[1]));
            }
            foreach (var keyValuePair in keyval)
            {
                postBox.Items.Add(keyValuePair);
            }

        }
    }
}
